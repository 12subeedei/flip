//
//  FlipGame.swift
//  Flip
//
//  Created by Subeedei Barkhasbadi on 2021.04.05.
//

import Foundation

struct FlipGame<CardContent> {
    var cards: Array<Card>
    
    func choose(card: Card) {
        print("card chose \(card)")
    }
    
    init(numberOfPairsOfCards: Int, cardContentFactory: (Int) -> CardContent) {
        cards = Array<Card>()
        for pairIndex in 0..<numberOfPairsOfCards {
            let content = cardContentFactory(pairIndex)
            cards.append(Card(content: content, id: pairIndex*2))
            cards.append(Card(content: content, id: pairIndex*2+1))
        }
    }
    
    struct Card: Identifiable {
        var isFaceUp: Bool = true
        var isMatched: Bool = false
        var content: CardContent
        var id: Int
    }

    
}

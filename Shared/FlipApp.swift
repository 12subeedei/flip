//
//  FlipApp.swift
//  Shared
//
//  Created by Sube Barkhasbadi on 2021.04.04.
//

import SwiftUI

@main
struct FlipApp: App {
    let game = EmojiFlipGame()
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: game)
        }
    }
}
    

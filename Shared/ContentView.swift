//
//  ContentView.swift
//  Shared
//
//  Created by Sube Barkhasbadi on 2021.04.04.
//

import SwiftUI

struct ContentView: View {
    var viewModel: EmojiFlipGame
    
    var body: some View {
        HStack {
            ForEach(viewModel.cards) { card in
                CardView(card: card).onTapGesture {
                    viewModel.choose(card: card)
                }
            }
        }
        .padding()
        .foregroundColor(.orange)
        .font(.largeTitle)
    }
        
}

struct CardView: View {
    var card: FlipGame<String>.Card
    
    var body: some View {
        if card.isFaceUp {
            ZStack {
                RoundedRectangle(cornerRadius: 10.0).fill(Color.white)
                RoundedRectangle(cornerRadius: 10.0).stroke(lineWidth: 3.0)
                Text(card.content)
            }
        } else {
            RoundedRectangle(cornerRadius: 10.0).fill(Color.orange)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: EmojiFlipGame())
    }
}

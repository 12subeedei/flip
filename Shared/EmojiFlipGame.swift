//
//  EmojiMemoryGame.swift
//  Flip
//
//  Created by Subeedei Barkhasbadi on 2021.04.05.
//

import Foundation
import SwiftUI


class EmojiFlipGame {
    
    private var model: FlipGame<String> = EmojiFlipGame.createFlipGame()
    
    static func createFlipGame() -> FlipGame<String> {
        let emojis: Array<String> = ["",""]
        return FlipGame<String>(numberOfPairsOfCards: emojis.count) { pairIndex in emojis[pairIndex] }
    }
    
    var cards: Array<FlipGame<String>.Card> {
        model.cards
    }
    
    // MARK: - Intent(s)
    
    func choose(card: FlipGame<String>.Card) {
        model.choose(card: card)
    }
}
